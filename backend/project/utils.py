from random import choice
from string import digits, ascii_lowercase


def gen_random_string(num_chars=40, char_type='an'):
    if char_type == 'al':
        chars = ascii_lowercase

    elif char_type == 'nu':
        chars = digits

    else:
        chars = ascii_lowercase + digits

    string = ''.join(choice(chars) for x in xrange(num_chars))

    return string
