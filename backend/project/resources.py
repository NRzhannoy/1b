import json
from time import sleep

from django.core.files.uploadedfile import InMemoryUploadedFile, TemporaryUploadedFile
from django.core.exceptions import (
    ObjectDoesNotExist, MultipleObjectsReturned,
)
from django.conf import settings

from tastypie import http, fields
from tastypie.resources import ModelResource


class AllowedMethods:
    DEFAULT = ['get', 'post', 'put', 'patch']
    ALL = ['get', 'post', 'put', 'patch', 'delete']
    READONLY = ['get']


def convert_post_to_VERB(request, verb):
    if request.method == verb:
        if hasattr(request, '_post'):
            del request._post
            del request._files

        try:
            request.method = 'POST'
            request._load_post_and_files()
            request.method = verb

        except AttributeError:
            request.META['REQUEST_METHOD'] = 'POST'
            request._load_post_and_files()
            request.META['REQUEST_METHOD'] = verb

        setattr(request, verb, request.POST)

    return request


def convert_post_to_patch(request):
    return convert_post_to_VERB(request, verb='PATCH')


class BaseResource(ModelResource):
    created = fields.DateTimeField('created', readonly=True)
    updated = fields.DateTimeField('updated', readonly=True)
    is_deleted = fields.BooleanField('is_deleted', readonly=True)

    def dispatch(self, request_type, request, **kwargs):
        delay = getattr(self._meta, 'delay', None)

        if not delay:
            delay = settings.API_DELAY

        if delay:
            sleep(delay)

        return super(BaseResource, self).dispatch(request_type, request, **kwargs)

    def patch_detail(self, request, **kwargs):
        '''
        NB: method is overriden to process multipart/form-data requests
        '''

        body = request.body
        request = convert_post_to_patch(request)

        basic_bundle = self.build_bundle(request=request)

        try:
            obj = self.cached_obj_get(
                bundle=basic_bundle,
                **self.remove_api_resource_names(kwargs)
            )

        except ObjectDoesNotExist:
            return http.HttpNotFound()

        except MultipleObjectsReturned:
            return http.HttpMultipleChoices('More than one resource is found at this URI.')

        bundle = self.build_bundle(obj=obj, request=request)
        bundle = self.full_dehydrate(bundle)
        bundle = self.alter_detail_data_to_serialize(request, bundle)

        deserialized = self.deserialize(request, body,
            format=request.META.get('CONTENT_TYPE', 'application/json')
        )
        self.update_in_place(request, bundle, deserialized)

        if not self._meta.always_return_data:
            return http.HttpAccepted()

        else:
            bundle.obj._prefetched_objects_cache = {}
            bundle = self.full_dehydrate(bundle)
            bundle = self.alter_detail_data_to_serialize(request, bundle)

            return self.create_response(request, bundle,
                response_class=http.HttpAccepted
            )

    def deserialize(self, request, data, format=None):
        '''
        NB: method is overriden to process multipart/form-data requests
        '''

        if not format:
            format = request.META.get('CONTENT_TYPE', 'application/json')

        if format == 'application/x-www-form-urlencoded':
            return request.POST

        if format.startswith('multipart'):
            data = request.POST.copy()
            data.update(request.FILES)

            data = data.dict()

            for key, val in data.items():
                if key.startswith('$'):
                    _key = key[1:]
                    data[_key] = data[key]

                    if not isinstance(data[_key], InMemoryUploadedFile) and not isinstance(data[_key], TemporaryUploadedFile):
                        del data[_key]

                if key.startswith('j$'):
                    _key = key[2:]
                    data[_key] = json.loads(data[key])

                if val == 'true':
                    data[key] = True

                elif val == 'false':
                    data[key] = False

            return data

        return super(BaseResource, self).deserialize(request, data, format)

    def _build_resource(self, request, obj):
        bundle = self.build_bundle(obj=obj, request=request)
        resource = self.full_dehydrate(bundle)

        return resource
