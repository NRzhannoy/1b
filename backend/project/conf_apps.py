# DJANGO
AUTH_USER_MODEL = 'accounts.User'

# DJANGO-ANYMAIL
ANYMAIL = {
    'MAILGUN_API_KEY': 'key-4c2df4a7f19aeff7dd42329c31c6056b',
    'MAILGUN_SENDER_DOMAIN': 'mg.1bplatform.com',
}

EMAIL_BACKEND = 'anymail.backends.mailgun.EmailBackend'
DEFAULT_FROM_EMAIL = '1B Team <contact@1bplatform.com>'

# DJANGO-CELERY
import djcelery
djcelery.setup_loader()
