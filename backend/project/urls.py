from django.conf.urls import url, include
from django.contrib import admin

from tastypie.api import Api

from accounts.resources import UserResource, CompanyResourse
from challenges.resources import ChallengeResource


api_v1 = Api(api_name='v1')

api_v1.register(UserResource())
api_v1.register(CompanyResourse())
api_v1.register(ChallengeResource())

urlpatterns = [
    url(r'^kzknlv6pzslq/', admin.site.urls),
    url(r'^', include(api_v1.urls)),
]
