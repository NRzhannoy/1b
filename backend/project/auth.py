from tastypie.authorization import Authorization
from tastypie.exceptions import Unauthorized


class AuthMessages:
    UNAUTHORIZED = 'permission_rejected'


class BaseAuthorization(Authorization):
    def set_user(self, bundle):
        self.user = bundle.request.user

    def raise_unauthorized(self):
        raise Unauthorized(AuthMessages.UNAUTHORIZED)

    def read_list(self, object_list, bundle):
        self.raise_unauthorized()

    def read_detail(self, object_list, bundle):
        self.raise_unauthorized()

    def create_list(self, object_list, bundle):
        self.raise_unauthorized()

    def create_detail(self, object_list, bundle):
        self.raise_unauthorized()

    def update_list(self, object_list, bundle):
        self.raise_unauthorized()

    def update_detail(self, object_list, bundle):
        self.raise_unauthorized()

    def delete_list(self, object_list, bundle):
        self.raise_unauthorized()

    def delete_detail(self, object_list, bundle):
        self.raise_unauthorized()
