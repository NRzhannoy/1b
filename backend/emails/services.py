# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string

from .tasks import send_email


class Mailer(object):
    def _send_email(self, email):
        send_email.delay(email)

    def send_test(self):
        msg = EmailMultiAlternatives(
            'Confirm your email for 1B',
            'Hello, Nick Ten. Please confirm your email. Regards, 1B team.',
            settings.DEFAULT_FROM_EMAIL,
            ['nrzhannoy@gmail.com']
        )

        self._send_email(msg)

    def send_confirmation(self, user):
        subject = 'Get started with 1B'

        data = {
            'name': user.name,
            'link': user.gen_confirmation_link(),
        }

        body = render_to_string(
            'emails/email_confirmation.txt',
            data
        )

        msg = EmailMultiAlternatives(
            subject, body, settings.DEFAULT_FROM_EMAIL, [user.email]
        )

        self._send_email(msg)
