from fabric.operations import run
from fabric.context_managers import cd, settings
from fabric.api import env


env.hosts = ['dev@138.197.69.255']
env.warn_only = True

PROJECT_DIR = '/home/dev/1b/'
VENV_DIR = PROJECT_DIR + 'env/bin/'
BACKEND_DIR = PROJECT_DIR + 'project/backend/'
FRONTEND_DIR = PROJECT_DIR + 'project/frontend/'


def deploy_front(mode='front'):
    with cd(FRONTEND_DIR):
        if mode == 'front':
            run('git pull')

        run('npm install')
        run('npm run build')


def deploy_back():
    PROMPTS = {
        "Type 'yes' to continue, or 'no' to cancel: ": 'yes',
    }

    with cd(BACKEND_DIR):
        run('git pull')
        run('{}pip install -r requirements.txt'.format(VENV_DIR))
        run('{}python {}manage.py migrate'.format(VENV_DIR, BACKEND_DIR))

        with settings(prompts=PROMPTS):
            run('{}python {}manage.py collectstatic'.format(VENV_DIR, BACKEND_DIR))

        run('sudo supervisorctl pid billion | xargs kill -HUP')
        run('sudo supervisorctl status billion')
        run('sudo supervisorctl restart billion-celery')


def deploy():
    deploy_back()
    deploy_front('both')
