# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import (
    PermissionsMixin, AbstractBaseUser, BaseUserManager
)
from django.contrib.postgres.fields import JSONField
from django.utils.text import slugify
from django.conf import settings

from tastypie.models import create_api_key

from project.base_models import Model
from project.utils import gen_random_string

from emails.services import Mailer


################
##  MANAGERS  ##
################

class UserManager(BaseUserManager):
    def create_user(self, email, password, **extra_fields):
        if not all([email, password, extra_fields['name']]):
            raise KeyError

        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        user.gen_confirmation_code()
        user.denormalize_token()

        Mailer().send_confirmation(user)

        return user

    def create_superuser(self, email, password, **extra_fields):
        user = self.create_user(email, password, **extra_fields)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)

        return user


class InviteManager(models.Manager):
    def check_obj(self, code):
        invite = self.filter(code=code).first()

        if invite:
            return {
                'success': True,
                'invite_id': invite.id,
                'owner_name': invite.user.name,
                'company_name': invite.company.name,
                'company_id': invite.company_id,
                'role': invite.type,
            }

        else:
            return {'success': False}

##############
##  MODELS  ##
##############

class User(Model, AbstractBaseUser, PermissionsMixin):
    MANAGER = 'ma'
    EMPLOYEE = 'em'

    ROLES = (
        (MANAGER, 'Manager'),
        (EMPLOYEE, 'Employee'),
    )

    ROLE_LIST = [MANAGER, EMPLOYEE]

    company = models.ForeignKey('accounts.Company', related_name='users', null=True, blank=True)
    invite = models.ForeignKey('accounts.Invite', related_name='users', blank=True, null=True)

    role = models.CharField(max_length=2, choices=ROLES, default=EMPLOYEE, db_index=True)

    name = models.CharField(max_length=40)
    email = models.EmailField(max_length=255, unique=True)

    confirmation_code = models.CharField(max_length=40, blank=True)
    token = models.CharField(max_length=300, blank=True)
    extra_data = JSONField(default=dict, blank=True)

    date_joined = models.DateTimeField(auto_now_add=True)

    is_email_confirmed = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False, db_index=True)
    is_staff = models.BooleanField(default=False)
    is_executive = models.BooleanField(default=False, db_index=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    objects = UserManager()

    def __unicode__(self):
        return 'U#{}'.format(self.id)

    def get_short_name(self):
        return self.name

    def get_full_name(self):
        return self.name

    def denormalize_token(self):
        self.token = '{}:{}'.format(self.email, self.api_key.key)
        self.save()

    @property
    def is_manager(self):
        return self.role == self.MANAGER

    @property
    def is_employee(self):
        return self.role == self.EMPLOYEE

    def gen_confirmation_code(self):
        self.confirmation_code = gen_random_string()
        self.save()

    def gen_confirmation_link(self):
        return '{}/confirm/?id={}&code={}:{}'.format(
            settings.FE_URL,
            self.id,
            self.company_id,
            self.confirmation_code
        )

    def confirm_email(self, code):
        company_id, code = code.split(':')

        if (
            str(self.company_id) == company_id and
            self.confirmation_code == code
        ):
            self.is_email_confirmed = True
            self.is_active = True
            self.save()

            return True

        return False

    def create_invites(self):
        if self.is_manager:
            for role in self.ROLE_LIST:
                if not Invite.objects.filter(
                    company=self.company, user=self, type=role
                ).exists():
                    invite = Invite.objects.create(
                        company=self.company,
                        user=self,
                        type=role,
                    )
                    invite.gen_code()

    def get_invites(self):
        invites = {}

        for invite in self.invites.all():
            invites[invite.type] = invite.code

        return invites

models.signals.post_save.connect(create_api_key, sender=User)


class Company(Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50, db_index=False, blank=True)

    is_active = models.BooleanField(default=False, db_index=True)

    def __unicode__(self):
        return 'Company #{}, {}'.format(self.id, self.name)

    def activate(self):
        self.slug = '{}-{}'.format(
            slugify(self.name, allow_unicode=True)[:20],
            self.id
        )
        self.is_active = True
        self.save()


class Invite(Model):
    company = models.ForeignKey(Company, related_name='invites')
    user = models.ForeignKey(User, related_name='invites')

    type = models.CharField(max_length=2, choices=User.ROLES, default=User.EMPLOYEE, db_index=True)

    code = models.CharField(max_length=100, blank=True)

    is_active = models.BooleanField(default=True, db_index=True)

    objects = InviteManager()

    def __unicode__(self):
        return 'Inv C#{}/U#{}'.format(self.company_id, self.user_id)

    def gen_code(self):
        code = gen_random_string()
        code = '{}:{}:{}'.format(self.company_id, self.user_id, code)

        self.code = code
        self.save()
