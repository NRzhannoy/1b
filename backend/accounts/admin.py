# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import User, Company, Invite


class UserAdmin(admin.ModelAdmin):
    list_display = ['email', 'role', 'is_email_confirmed', 'created']


class CompanyAdmin(admin.ModelAdmin):
    list_display = ['name', 'created']


class InviteAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', 'type', 'created']


admin.site.register(User, UserAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Invite, InviteAdmin)
