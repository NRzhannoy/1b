# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from time import sleep

from django.conf.urls import url
from django.contrib.auth import authenticate, login

from project.resources import BaseResource, AllowedMethods

from tastypie import fields
from tastypie.authentication import ApiKeyAuthentication
from tastypie.utils import trailing_slash
from tastypie.exceptions import BadRequest

from .models import User, Company, Invite
from .auth import UserAuthorization, CompanyAuthorization


class UserResource(BaseResource):
    role = fields.CharField('role', readonly=True)

    class Meta:
        queryset = User.objects.filter(
            is_deleted=False, is_active=True
        )
        allowed_methods = AllowedMethods.ALL
        resource_name = 'user'
        authentication = ApiKeyAuthentication()
        authorization = UserAuthorization()
        always_return_data = True

        excludes = [
            'confirmation_code', 'token',
            'is_superuser', 'is_staff',
            'password'
        ]

    def prepend_urls(self):
        return [
            url(r'^(?P<resource_name>{})/register{}$'.format(self._meta.resource_name, trailing_slash()),
                self.wrap_view('register'), name='api_register'),
            url(r'^(?P<resource_name>{})/login{}$'.format(self._meta.resource_name, trailing_slash()),
                self.wrap_view('login'), name='api_login'),
            url(r'^(?P<resource_name>{})/confirm{}$'.format(self._meta.resource_name, trailing_slash()),
                self.wrap_view('confirm'), name='api_confirm'),
            url(r'^(?P<resource_name>{})/check-invite{}$'.format(self._meta.resource_name, trailing_slash()),
                self.wrap_view('check_invite'), name='api_check_invite'),
            url(r'^(?P<resource_name>{})/join{}$'.format(self._meta.resource_name, trailing_slash()),
                self.wrap_view('join'), name='api_join'),
        ]

    def full_dehydrate(self, bundle, for_list=False):
        if not for_list:
            if bundle.obj.id == bundle.request.user.id:
                bundle.data['invites'] = bundle.request.user.get_invites()

        return super(UserResource, self).full_dehydrate(bundle, for_list)

    def _login(self, request, user):
        login(request, user)
        user_resource = self._build_resource(request, user)

        return self.create_response(request, {
            'success': True,
            'user': user_resource,
            'token': user.token,
            'company_name': user.company.name,
        })

    def _register(self, request, data, login=True):
        user = User.objects.create_user(**data['user'])
        user.backend = 'django.contrib.auth.backends.ModelBackend'

        if login:
            return self._login(request, user)

        else:
            return self.create_response(request, {'success': True})

    def _user_check(self, data):
        if User.objects.filter(email=data['user']['email']).exists():
            raise BadRequest('email_exists')

    def register(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        data = self.deserialize(request, request.body)
        self._user_check(data)

        company = Company.objects.create(**data['company'])
        company.activate()

        data['user']['role'] = User.MANAGER
        data['user']['is_executive'] = True
        data['user']['company_id'] = company.id
        data['user']['is_active'] = True

        return self._register(request, data)

    def confirm(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        data = self.deserialize(request, request.body)

        user = User.objects.get(id=data['user_id'])
        result = user.confirm_email(data['code'])

        if not result:
            raise BadRequest('invalid_data')

        return self._login(request, user)

    def login(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        data = self.deserialize(request, request.body)

        user = authenticate(
            email=data['email'],
            password=data['password']
        )

        if user:
            return self._login(request, user)
        else:
            raise BadRequest('no_user')

    def check_invite(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        data = self.deserialize(request, request.body)
        result = Invite.objects.check_obj(data['code'])
        return self.create_response(request, result)

    def join(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        data = self.deserialize(request, request.body)
        self._user_check(data)

        invite_data = Invite.objects.check_obj(data['code'])

        if not invite_data['success']:
            raise BadRequest('invalid_code')

        data['user']['role'] = invite_data['role']
        data['user']['company_id'] = invite_data['company_id']
        data['user']['invite_id'] = invite_data['invite_id']
        data['user']['is_active'] = False

        return self._register(request, data, login=False)


class CompanyResourse(BaseResource):
    is_active = fields.BooleanField('is_active', readonly=True)

    class Meta:
        queryset = Company.objects.filter(is_deleted=False, is_active=True)
        allowed_methods = AllowedMethods.DEFAULT
        resource_name = 'company'
        authentication = ApiKeyAuthentication()
        authorization = CompanyAuthorization()
