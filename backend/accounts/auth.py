from project.auth import BaseAuthorization


class UserAuthorization(BaseAuthorization):
    def read_list(self, object_list, bundle):
        self.set_user(bundle)
        return object_list.filter(company_id=self.user.company_id)

    def read_detail(self, object_list, bundle):
        self.set_user(bundle)

        if self.user.company_id == bundle.obj.company_id:
            return True

        self.raise_unauthorized()

    def update_detail(self, object_list, bundle):
        self.set_user(bundle)

        if (
            bundle.obj.id == self.user.id or
            (
                self.user.is_manager and
                self.user.company_id == bundle.obj.company_id
            )
        ):
            return True

        self.raise_unauthorized()

    def delete_detail(self, object_list, bundle):
        self.set_user(bundle)

        if (
            bundle.obj.id != self.user.id and
            (
                self.user.is_manager and
                self.user.company_id == bundle.obj.company_id
            )
        ):
            return True

        self.raise_unauthorized()


class CompanyAuthorization(BaseAuthorization):
    def read_detail(self, object_list, bundle):
        self.set_user(bundle)

        if self.user.company_id == bundle.obj.id:
            return True

        self.raise_unauthorized()
