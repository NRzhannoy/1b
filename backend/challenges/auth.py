from project.auth import BaseAuthorization


class ChallengeAuthorization(BaseAuthorization):
    def read_list(self, object_list, bundle):
        self.set_user(bundle)
        return object_list.filter(company_id=self.user.company_id)

    def read_detail(self, object_list, bundle):
        self.set_user(bundle)
        if self.user.company_id == bundle.obj.company_id:
            return True

        self.raise_unauthorized()

    def create_detail(self, object_list, bundle):
        self.set_user(bundle)
        if self.user.is_manager:
            return True

        self.raise_unauthorized()
