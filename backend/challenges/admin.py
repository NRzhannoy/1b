# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Challenge


class ChallengeAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', 'created']


admin.site.register(Challenge, ChallengeAdmin)
