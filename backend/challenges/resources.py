# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from django.utils import timezone

from project.resources import BaseResource, AllowedMethods

from tastypie import fields
from tastypie.authentication import ApiKeyAuthentication
from tastypie.utils import trailing_slash
from tastypie.exceptions import BadRequest

from .models import Challenge
from .auth import ChallengeAuthorization


class ChallengeResource(BaseResource):
    user = fields.ForeignKey('accounts.resources.UserResource', 'user', readonly=True)
    company = fields.ForeignKey('accounts.resources.CompanyResource', 'company', readonly=True)
    is_active = fields.BooleanField('is_active', readonly=True)

    class Meta:
        queryset = Challenge.objects.filter(is_deleted=False)
        allowed_methods = AllowedMethods.DEFAULT
        resource_name = 'challenge'
        authentication = ApiKeyAuthentication()
        authorization = ChallengeAuthorization()

    def obj_create(self, bundle, **kwargs):
        user = bundle.request.user

        kwargs.update({
            'user': user,
            'company': user.company,
            'dt_start': timezone.now(),
        })

        return super(ChallengeResource, self).obj_create(bundle, **kwargs)
