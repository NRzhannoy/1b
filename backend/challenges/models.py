# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from project.base_models import Model


class Challenge(Model):
    user = models.ForeignKey('accounts.User', related_name='challenges')
    company = models.ForeignKey('accounts.Company', related_name='challenges')

    title = models.CharField(max_length=200)
    reward = models.CharField(max_length=200, blank=True)
    background = models.TextField(blank=True)
    result = models.TextField()
    criteria = models.TextField(blank=True)
    extra_details = models.TextField(blank=True)

    dt_start = models.DateTimeField(blank=True, null=True)
    dt_end = models.DateTimeField(blank=True, null=True)

    is_active = models.BooleanField(default=True, db_index=True)

    def __unicode__(self):
        return 'C#{}'.format(self.id)
