const dataTypes = {
  INIT_HTTP: 'initHttp',
  INIT_RESOURCES: 'initResources',
  UPDATE_USER: 'updateUser',
  LOG_IN: 'logIn',
  LOG_OUT: 'logOut',
  DO_AUTH: 'doAuth',
  REGISTER_USER: 'registerUser',
  RESET_STATE: 'resetState',
  CONFIRM_USER: 'confirmUser',
  GET_USERS: 'getUsers',
  SET_USERS: 'setUsers',
  DELETE_USER: 'deleteUser',
  CHECK_INVITE: 'checkInvite',
  JOIN: 'join',
  CREATE_CHALLENGE: 'createChallenge',
}

const uiTypes = {
  UI_ENABLE_LOADING: 'uiEnableLoading',
  UI_DISABLE_LOADING: 'uiDisableLoading',
  UI_SHOW_ERROR: 'uiShowError',
  UI_SHOW_SUCCESS: 'uiShowSuccess',
  UI_PROCESS_GENERIC_ERROR: 'uiProcessGenericError',
  UI_PROCESS_REG_ERROR: 'uiProcessRegError',
}

export default Object.assign({}, dataTypes, uiTypes)
