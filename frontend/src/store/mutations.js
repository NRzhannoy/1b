import Vue from 'vue'

import initialState from './state'
import types from './types'
import createUserService from '../services/user'
import createChallengeService from '../services/challenge'

export default {
  [types.RESET_STATE] (state) {
    state = Object.assign(state, initialState)
  },

  [types.INIT_HTTP] (state) {
    const hostname = document.location.hostname
    let baseURL = ''
    let siteUrl = ''

    if (
      hostname.indexOf('localhost') > -1 ||
      hostname.indexOf('ngrok') > -1
    ) {
      baseURL = 'http://127.0.0.1:8000/v1/'
      siteUrl = 'http://localhost:8080/'
    } else {
      baseURL = 'https://api.1bplatform.com/v1/'
      siteUrl = 'https://1bplatform.com/'
    }

    const headers = {}

    if (state.user) {
      headers.Authorization = `ApiKey ${state.user.token}`
    }

    state.httpConf = { baseURL, headers }
    state.siteUrl = siteUrl
  },

  [types.UPDATE_USER] (state, payload) {
    state.user = payload
  },

  [types.LOG_OUT] (state) {
    state = Object.assign(state, initialState)
  },

  [types.UI_ENABLE_LOADING] (state) {
    state.ui.isLoading = true
  },

  [types.UI_DISABLE_LOADING] (state) {
    state.ui.isLoading = false
  },

  [types.UI_SHOW_ERROR] (state, payload) {
    const duration = payload.duration ? payload.duration : 4000

    Vue.toasted.error(payload.message, {
      duration: duration,
      icon: 'exclamation-circle',
    })
  },

  [types.UI_SHOW_SUCCESS] (state, payload) {
    const duration = payload.duration ? payload.duration : 4000

    Vue.toasted.success(payload.message, {
      duration: duration,
      icon: 'check-circle',
    })
  },

  [types.INIT_RESOURCES] (state) {
    state.User = createUserService(state.httpConf)
    state.Challenge = createChallengeService(state.httpConf)
  },

  [types.SET_USERS] (state, payload) {
    state.users = payload.users
  },

  [types.DELETE_USER] (state, payload) {
    state.users = state.users.filter(function (user) {
      return user.id !== payload.id
    })
  }
}
