export default {
  isManager: state => {
    return state.user.role === 'ma'
  },
}
