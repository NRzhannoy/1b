export default {
  user: null,
  httpConf: null,
  siteUrl: null,
  users: [],
  routesWithAuth: ['intro', 'join', 'email-confirmation', 'landing'],
  ui: {
    isLoading: false,
  },
}
