import types from './types'

export default {
  [types.UPDATE_USER] ({ commit, state }, payload) {
    state.User.get(`${payload.id}/`)
      .then(function (res) {
        const user = Object.assign({}, state.user, res.data)
        commit(types.UPDATE_USER, user)
      })
  },

  [types.UI_PROCESS_GENERIC_ERROR] ({ commit }, payload) {
    commit(types.UI_DISABLE_LOADING)
    commit(types.UI_SHOW_ERROR, {
      message: 'Something went wrong, please try again later',
    })

    console.log('GENERIC ERROR', payload.response.data)
  },

  [types.UI_PROCESS_REG_ERROR] ({ commit, dispatch }, payload) {
    commit(types.UI_DISABLE_LOADING)
    const errData = payload.response.data

    if (errData.error === 'email_exists') {
      commit(types.UI_SHOW_ERROR, {
        message: 'User with this email already exists',
      })
    } else {
      dispatch(types.UI_PROCESS_GENERIC_ERROR, payload)
    }
  },

  [types.REGISTER_USER] ({ commit, state, dispatch }, payload) {
    commit(types.UI_ENABLE_LOADING)

    return new Promise((resolve, reject) => {
      state.User
        .post('register/', {
          user: payload.user,
          company: payload.company,
        })
        .then(function (res) {
          commit(types.UI_DISABLE_LOADING)
          dispatch(types.DO_AUTH, res)
          resolve()
        })
        .catch(function (err) {
          dispatch(types.UI_PROCESS_REG_ERROR, err)
          reject(err)
        })
    })
  },

  [types.LOG_IN] ({ state, commit, dispatch }, payload) {
    commit(types.UI_ENABLE_LOADING)

    return new Promise((resolve, reject) => {
      state.User
        .post('login/', payload.user)
        .then(function (res) {
          commit(types.UI_DISABLE_LOADING)
          dispatch(types.DO_AUTH, res)
          resolve()
        }).catch(function (err) {
          commit(types.UI_DISABLE_LOADING)
          const errData = err.response.data

          if (errData.error === 'no_user') {
            commit(types.UI_SHOW_ERROR, {
              message: 'Credentials are invalid or user is inactive',
            })
          } else {
            dispatch(types.UI_PROCESS_GENERIC_ERROR, err)
          }
          reject(err)
        })
    })
  },

  [types.DO_AUTH] ({ commit }, payload) {
    const user = payload.data.user
    user.token = payload.data.token
    user.companyName = payload.data.company_name

    commit(types.UPDATE_USER, user)
  },

  [types.CONFIRM_USER] ({ state, commit, dispatch }, payload) {
    commit(types.UI_ENABLE_LOADING)

    return new Promise((resolve, reject) => {
      state.User
        .post('confirm/', {
          user_id: payload.id,
          code: payload.code,
        })
        .then(function (res) {
          commit(types.UI_DISABLE_LOADING)
          dispatch(types.DO_AUTH, res)
          resolve()
        })
        .catch(function (err) {
          commit(types.UI_DISABLE_LOADING)
          commit(types.UI_SHOW_ERROR, 'Invalid data')
          reject(err)
        })
    })
  },

  [types.GET_USERS] ({ state, commit, dispatch }) {
    commit(types.UI_ENABLE_LOADING)

    return new Promise((resolve, reject) => {
      state.User.get()
        .then(function (res) {
          commit(types.UI_DISABLE_LOADING)
          commit(types.SET_USERS, { users: res.data.objects })
          resolve()
        })
        .catch(function (err) {
          dispatch(types.UI_PROCESS_GENERIC_ERROR, err)
          reject(err)
        })
    })
  },

  [types.DELETE_USER] ({ state, commit, dispatch }, payload) {
    commit(types.UI_ENABLE_LOADING)

    return new Promise((resolve, reject) => {
      state.User.delete(`${payload.id}/`)
        .then(function (res) {
          commit(types.UI_DISABLE_LOADING)
          commit(types.DELETE_USER, { id: payload.id })
          commit(types.UI_SHOW_SUCCESS, { message: 'User was successfully deleted' })
          resolve()
        })
        .catch(function (err) {
          dispatch(types.UI_PROCESS_GENERIC_ERROR, err)
          reject(err)
        })
    })
  },

  [types.CHECK_INVITE] ({ state, commit, dispatch }, payload) {
    commit(types.UI_ENABLE_LOADING)

    return new Promise((resolve, reject) => {
      state.User.post('check-invite/', { code: payload.code })
        .then(function (res) {
          commit(types.UI_DISABLE_LOADING)
          resolve(res.data)
        })
        .catch(function (err) {
          dispatch(types.UI_PROCESS_GENERIC_ERROR, err)
          reject(err)
        })
    })
  },

  [types.JOIN] ({ state, commit, dispatch }, payload) {
    commit(types.UI_ENABLE_LOADING)

    return new Promise((resolve, reject) => {
      state.User.post('join/', payload)
        .then(function (res) {
          commit(types.UI_DISABLE_LOADING)
          resolve()
        })
        .catch(function (err) {
          dispatch(types.UI_PROCESS_REG_ERROR, err)
          reject(err)
        })
    })
  },

  [types.CREATE_CHALLENGE] ({ state, commit, dispatch }, payload) {
    commit(types.UI_ENABLE_LOADING)

    return new Promise((resolve, reject) => {
      state.Challenge.post('/', payload)
        .then(function (res) {
          commit(types.UI_DISABLE_LOADING)
          commit(types.UI_SHOW_SUCCESS, { message: 'You\'ve successfully created a challenge' })
          resolve()
        })
        .catch(function (err) {
          dispatch(types.UI_PROCESS_REG_ERROR, err)
          reject(err)
        })
    })
  },
}
