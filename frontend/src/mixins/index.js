export default {
  managersOnlyMixin: {
    created () {
      if (!this.$store.getters.isManager) {
        this.$router.push({ name: 'main' })
      }
    }
  },
}
