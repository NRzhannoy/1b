import Vue from 'vue'
import Router from 'vue-router'

import IntroPage from '@/pages/IntroPage'
import LandingPage from '@/pages/LandingPage'
import StaffPage from '@/pages/StaffPage'
import JoinPage from '@/pages/JoinPage'
import ChallengeNewPage from '@/pages/ChallengeNewPage'
import MainPage from '@/pages/MainPage'
import ChallengesMyPage from '@/pages/ChallengesMyPage'
import EmailConfirmationPage from '@/pages/EmailConfirmationPage'
import ChallengePage from '@/pages/ChallengePage'

Vue.use(Router)

export default new Router({
  mode: 'history',

  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else if (to.hash) {
      return { selector: to.hash }
    } else {
      return { x: 0, y: 0 }
    }
  },

  routes: [
    {
      path: '/',
      name: 'landing',
      component: LandingPage,
    },
    {
      path: '/intro-old/',
      name: 'intro',
      component: IntroPage,
    },
    {
      path: '/staff/',
      name: 'staff',
      component: StaffPage,
    },
    {
      path: '/join/:code/',
      name: 'join',
      component: JoinPage,
    },
    {
      path: '/challenge/new/',
      name: 'challenge-new',
      component: ChallengeNewPage,
    },
    {
      path: '/challenges/',
      name: 'main',
      component: MainPage,
    },
    {
      path: '/challenges/my/',
      name: 'challenges-my',
      component: ChallengesMyPage,
    },
    {
      path: '/confirm/',
      name: 'email-confirmation',
      component: EmailConfirmationPage,
    },
    {
      path: '/challenges/42/',
      name: 'challenge',
      component: ChallengePage,
    },
  ],
})
