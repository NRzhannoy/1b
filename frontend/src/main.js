// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

import Toasted from 'vue-toasted'
import VeeValidate from 'vee-validate'
import VueLocalStorage from 'vue-ls'
import VueScrollTo from 'vue-scrollto'

import App from './App'
import router from './router'
import store from './store'

Vue.config.productionTip = false

const isMobile = screen.width < 769

Vue.use(Toasted, {
  fullWidth: isMobile,
  fitToScreen: isMobile,
  position: 'top-center',
  iconPack: 'fontawesome',
  singleton: true,
})
Vue.use(VeeValidate, { events: 'blur' })
Vue.use(VueLocalStorage, { namespace: '1b__' })
Vue.use(VueScrollTo, { duration: 300 })

Vue.prototype.$isMobile = isMobile

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
})
