import axios from 'axios'

export default function (conf) {
  const resource = axios.create({
    baseURL: conf.baseURL + 'challenge/',
    headers: conf.headers,
  })

  return resource
}
