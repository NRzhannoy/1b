import axios from 'axios'

export default function (conf) {
  const resource = axios.create({
    baseURL: conf.baseURL + 'user/',
    headers: conf.headers,
  })

  return resource
}
